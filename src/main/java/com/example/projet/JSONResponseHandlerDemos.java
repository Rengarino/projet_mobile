package com.example.projet;

import android.util.JsonReader;

import com.example.projet.data.Artefact;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class JSONResponseHandlerDemos {
    private static final String TAG = JSONResponseHandlerArtefact.class.getSimpleName();

    private String demo;
    private String artefactId;
    private List<String> demoList;


    public JSONResponseHandlerDemos(String artefactId, List<String> demoList) {
        this.artefactId = artefactId;
        this.demoList = demoList; }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readArtefacts(reader);
        } finally {
            reader.close();
        }
    }

    public void readArtefacts(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            readArrayArtefacts(reader);
            demoList.add(demo);
        }
        reader.endObject();

    }

    private void readArrayArtefacts(JsonReader reader) throws IOException {
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(artefactId)) {
                demo = reader.nextString();
            }
            else {
                reader.skipValue();
            }
        }
    }
}
