package com.example.projet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.projet.data.Artefact;
import com.example.projet.webservice.WebServiceUrl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class Request {
    JSONResponseHandlerArtefact JSONartefact;
    JSONResponseHandlerDemos JSONdemos;

    public List<Artefact> getArtefactsInfo(List<Artefact> artefactList) {
        JSONartefact = new JSONResponseHandlerArtefact(artefactList);
        URL url = null;

        try {
            url = WebServiceUrl.buildSearchArtefact();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            JSONartefact.readJsonStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }
        return artefactList;
    }

    public Bitmap getThumbnailFromURL(String idArtefact)
    {
        Bitmap icon = null;
        URL url = null;
        try {
            url = WebServiceUrl.buildSearchArtefactThumbnail(idArtefact);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            icon = BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }
        return icon;
    }

    public Bitmap getImageFromURL(String idArtefact, String imageID) {
        Bitmap icon = null;
        URL url = null;
        try {
            url = WebServiceUrl.buildSearchArtefactImage(idArtefact, imageID);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            icon = BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }
        return icon;
    }

    public List<String> getDemos(String artefactId, List<String> demoList) {
        JSONdemos = new JSONResponseHandlerDemos(artefactId, demoList);
        URL url = null;

        try {
            url = WebServiceUrl.buildSearchDemos();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            JSONdemos.readJsonStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }
        return demoList;
    }
}
