package com.example.projet.webservice;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    // constant string used as s parameter for table
    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // Build URL to get information for all artefacts
    private static final String SEARCH_ARTEFACT = "catalog";
    public static URL buildSearchArtefact() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ARTEFACT);
        URL url = new URL(builder.build().toString());
        return url;
    }

    //Build URL to get an artefact thumbnail
    //https://demo-lia.univ-avignon.fr/cerimuseum/items/ci6/thumbnail
    private static final String ITEMS = "items";
    private static final String THUMBNAIL = "thumbnail";

    public static URL buildSearchArtefactThumbnail(String idArtefact) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(idArtefact)
                .appendPath(THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

    //Build URL to get an artefact image
    //https://demo-lia.univ-avignon.fr/cerimuseum/items/ci6/images/IMG_1617
    private static final String IMAGES = "images";

    public static URL buildSearchArtefactImage(String idArtefact, String imageID) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(idArtefact)
                .appendPath(IMAGES)
                .appendPath(imageID.trim());
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Build URL to get information for all artefacts
    private static final String SEARCH_DEMOS = "demos";
    public static URL buildSearchDemos() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_DEMOS);
        URL url = new URL(builder.build().toString());
        return url;
    }

}
