package com.example.projet.data;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

public class Artefact implements Parcelable {

    public static final String TAG = Artefact.class.getSimpleName();

    private long id; // used for the _id column of the db helper

    private String name;
    private String description;

    private String idArtefact; //used for the web service
    private int year;
    private String brand;
    private String working;

    private List<String> pictures = new ArrayList<>();
    private List<String> categories = new ArrayList<>();
    private List<String> technicalDetails = new ArrayList<>();
    private List<String> timeFrame = new ArrayList<>();

    private String allCategories;
    private String allTechnicalDetails;
    private String allPictures;
    private String allTimeFrame;

    public Artefact(String idArtefact) { this.idArtefact = idArtefact;}

    public Artefact(String name, String brand) {
        this.name = name;
        this.brand = brand;
    }

    public Artefact(long id, String name, String brand) {
        this.id = id;
        this.name = name;
        this.brand = brand;
    }

    public Artefact(long id, String name, String brand, String categories, String technicalDetails) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.allCategories = categories;
        this.allTechnicalDetails = technicalDetails;
    }

    public Artefact(long id, String name, String brand, String categories, String technicalDetails, String idArtefact, String working, String description, int year, String pictures, String timeFrame) {
        this.id = id;
        this.idArtefact = idArtefact;
        this.name = name;
        this.brand = brand;
        this.working = working;
        this.description = description;
        this.year = year;
        this.allTechnicalDetails = technicalDetails;
        this.allTimeFrame = timeFrame;
        this.allPictures = pictures;
        this.allCategories = categories;
    }

    public String getAllCategories() {
        return allCategories;
    }

    public String getAllTechnicalDetails() {
        return allTechnicalDetails;
    }

    public String getAllPictures() {
        return allPictures;
    }

    public String getAllTimeFrame() {
        return allTimeFrame;
    }

    /*public void setStringToCategories(String allCategories) {
        String[] result = allCategories.split("|");
        for(String string : result)
        {
            this.categories.add(string);
        }
    }*/

    public String getIdArtefact() {
        return idArtefact;
    }

    public long getId() { return id; }

    public String getName() { return name; }

    public String getDescription() { return description; }

    public String getTimeFrame() {
        String allTimeFrame = "";
        for(String time : this.timeFrame) {
            allTimeFrame += time + "-";
        }
        return allTimeFrame;
    }

    public String getPictures() {
        String allPictures = "";
        for(String picture : this.pictures) {
            allPictures += picture + " | ";
        }
        return allPictures;
    }

    public int getYear() { return year; }

    public String getBrand() { return brand; }

    public String getTechnicalDetails() {
        String allTechnicalDetails = "";
        for(String tech : this.technicalDetails) {
            allTechnicalDetails += tech + "|";
        }
        return allTechnicalDetails;
    }

    public String getCategories() {
        String allCategories = "";
        for(String categorie : this.categories) {
            allCategories += categorie + "|";
        }
        return allCategories;
    }

    public String getWorking() { return working; }

    public void setId(long id) { this.id = id; }

    public void setName(String name) { this.name = name; }

    public void setBrand(String brand) { this.brand = brand; }

    public void setIdArtefact(String idArtefact) {
        this.idArtefact = idArtefact;
    }

    public void setTechnicalDetails(String technicalDetail) {
        this.technicalDetails.add(technicalDetail);
    }

    public void setTimeFrame(String time) {
        this.timeFrame.add(time);
    }

    public void setCategories(String categorie) {
        this.categories.add(categorie);
    }

    public void setPictures(String picture) {
        this.pictures.add(picture);
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(allCategories);
        dest.writeString(description);
        dest.writeString(allTimeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        dest.writeString(idArtefact);
        dest.writeString(allTechnicalDetails);
        dest.writeString(working);
        dest.writeString(allPictures);
    }

    //Bien respecter l'ordre de writeToParcel
    public Artefact(Parcel in) {
        id = in.readLong();
        name = in.readString();
        allCategories = in.readString();
        description = in.readString();
        allTimeFrame = in.readString();
        year = in.readInt();
        brand = in.readString();
        idArtefact = in.readString();
        allTechnicalDetails = in.readString();
        working = in.readString();
        allPictures = in.readString();
    }

    @NonNull
    @Override
    public String toString() {
        return "Nom : "+this.name+" Marque : "+this.brand + " Categories : " +this.allCategories + " Tech : " +this.allTechnicalDetails + "TimeFrame " + this.allTimeFrame;
    }

    public static final Creator<Artefact> CREATOR = new Creator<Artefact>() {
        @Override
        public Artefact createFromParcel(Parcel in) {
            return new Artefact(in);
        }

        @Override
        public Artefact[] newArray(int size) {
            return new Artefact[size];
        }
    };
}
