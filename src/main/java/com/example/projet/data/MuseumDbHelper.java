package com.example.projet.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class MuseumDbHelper extends SQLiteOpenHelper {

    private static final String TAG = MuseumDbHelper.class.getSimpleName();
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "museum.db";

    public static final String TABLE_NAME = "museum";

    public static final String _ID = "_id";
    public static final String COLUMN_ARTEFACT_NAME = "name";
    public static final String COLUMN_ARTEFACT_BRAND = "brand";
    public static final String COLUMN_ARTEFACT_CATEGORIES = "categories";
    public static final String COLUMN_ARTEFACT_TECHNICAL_DETAILS = "technicalDetails";

    public static final String COLUMN_ARTEFACT_ID = "idArtefact";
    public static final String COLUMN_ARTEFACT_WORKING = "working";
    public static final String COLUMN_ARTEFACT_DESCRIPTION = "description";
    public static final String COLUMN_ARTEFACT_YEAR = "year";
    public static final String COLUMN_ARTEFACT_PICTURES = "pictures";
    public static final String COLUMN_ARTEFACT_TIMEFRAME = "timeframe";

    public static final String SEARCH_TABLE_NAME = "search";
    public static final String CATEGORIES_TABLE_NAME = "categories";

    public MuseumDbHelper(Context context) { super(context, DATABASE_NAME, null , DATABASE_VERSION); }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_MUSEUM_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_NAME + " TEXT NOT NULL, " +
                COLUMN_ARTEFACT_BRAND + " TEXT, " +
                COLUMN_ARTEFACT_CATEGORIES + " TEXT, " +
                COLUMN_ARTEFACT_TECHNICAL_DETAILS + " TEXT, " +
                COLUMN_ARTEFACT_ID + " TEXT, " +
                COLUMN_ARTEFACT_WORKING + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION + " TEXT, " +
                COLUMN_ARTEFACT_YEAR + " INTEGER, " +
                COLUMN_ARTEFACT_PICTURES + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME + " TEXT, " +

                // To assure the application have just one team entry per
                // artefact name, it's created as UNIQUE
                " UNIQUE(" + COLUMN_ARTEFACT_NAME + ") ON CONFLICT ROLLBACK)";

        db.execSQL(SQL_CREATE_MUSEUM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion != oldVersion) {
            db.execSQL("drop table if exists " + TABLE_NAME);
            onCreate(db);
        }
    }

    public void dropCategoriesTable(SQLiteDatabase db) {
        db.execSQL("drop table if exists " + CATEGORIES_TABLE_NAME);
        final String SQL_CREATE_CATEGORIES_TABLE = "CREATE TABLE " + CATEGORIES_TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_NAME + " TEXT NOT NULL, " +
                COLUMN_ARTEFACT_BRAND + " TEXT, " +
                COLUMN_ARTEFACT_CATEGORIES + " TEXT, " +
                COLUMN_ARTEFACT_TECHNICAL_DETAILS + " TEXT, " +
                COLUMN_ARTEFACT_ID + " TEXT, " +
                COLUMN_ARTEFACT_WORKING + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION + " TEXT, " +
                COLUMN_ARTEFACT_YEAR + " INTEGER, " +
                COLUMN_ARTEFACT_PICTURES + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME + " TEXT, " +


                " UNIQUE(" + _ID + ") ON CONFLICT ROLLBACK)";
        db.execSQL(SQL_CREATE_CATEGORIES_TABLE);
    }

    public void dropSearchTable(SQLiteDatabase db) {
        db.execSQL("drop table if exists " + SEARCH_TABLE_NAME);
        final String SQL_CREATE_SEARCH_TABLE = "CREATE TABLE " + SEARCH_TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_NAME + " TEXT NOT NULL, " +
                COLUMN_ARTEFACT_BRAND + " TEXT, " +
                COLUMN_ARTEFACT_CATEGORIES + " TEXT, " +
                COLUMN_ARTEFACT_TECHNICAL_DETAILS + " TEXT, " +
                COLUMN_ARTEFACT_ID + " TEXT, " +
                COLUMN_ARTEFACT_WORKING + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION + " TEXT, " +
                COLUMN_ARTEFACT_YEAR + " INTEGER, " +
                COLUMN_ARTEFACT_PICTURES + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME + " TEXT, " +


                " UNIQUE(" + COLUMN_ARTEFACT_NAME + ") ON CONFLICT ROLLBACK)";
        db.execSQL(SQL_CREATE_SEARCH_TABLE);
    }

    public void dropTable(SQLiteDatabase db) {
        db.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Fills ContentValues result from a Artefact object
     */
    private ContentValues fill(Artefact artefact) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ARTEFACT_NAME, artefact.getName());
        values.put(COLUMN_ARTEFACT_BRAND,artefact.getBrand());
        values.put(COLUMN_ARTEFACT_CATEGORIES, artefact.getCategories());
        values.put(COLUMN_ARTEFACT_TECHNICAL_DETAILS, artefact.getTechnicalDetails());
        values.put(COLUMN_ARTEFACT_ID, artefact.getIdArtefact());
        values.put(COLUMN_ARTEFACT_WORKING, artefact.getWorking());
        values.put(COLUMN_ARTEFACT_DESCRIPTION, artefact.getDescription());
        values.put(COLUMN_ARTEFACT_YEAR, artefact.getYear());
        values.put(COLUMN_ARTEFACT_PICTURES, artefact.getPictures());
        values.put(COLUMN_ARTEFACT_TIMEFRAME, artefact.getTimeFrame());
        return values;
    }

    /**
     * Adds a new team
     * @return  true if the team was added to the table ; false otherwise (case when the pair (team, championship) is
     * already in the data base)
     */
    public boolean addArtefact(Artefact artefact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(artefact);

        //Log.d(TAG, "adding: "+artefact.getName() + " brand : " + artefact.getBrand() + " categories : " + artefact.getCategories() + " technical details : " + artefact.getTechnicalDetails());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Returns a cursor on all the artefacts of the data base
     */
    public Cursor fetchAllArtefacts() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_ARTEFACT_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllArtefacts()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Returns a cursor on all the artefacts of the data base order by year
     */
    public Cursor fetchAllArtefactsFromYear() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_ARTEFACT_YEAR +" ASC", null);

        Log.d(TAG, "call fetchAllArtefactsFromYear()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Returns a list on all the teams of the data base
     */
    public List<Artefact> getAllArtefacts() {
        SQLiteDatabase db = this.getWritableDatabase();
        List<Artefact> res = new ArrayList<>();
        final Cursor cursor = fetchAllArtefacts();
        if(cursor !=null) { //Obligé de le faire dans les 2 sinon il passe la première information
            cursor.moveToFirst();
            res.add(cursorToArtefact(cursor));
        }

        while(cursor.moveToNext()) {
            res.add(cursorToArtefact(cursor));
        }
        return res;
    }

    public Artefact cursorToArtefact(Cursor cursor) {
        Artefact artefact = new Artefact(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_BRAND)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_CATEGORIES)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_TECHNICAL_DETAILS)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_WORKING)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_DESCRIPTION)),
                cursor.getInt(cursor.getColumnIndex(COLUMN_ARTEFACT_YEAR)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_PICTURES)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_TIMEFRAME))
        );
        //System.out.println(artefact.toString());
        return artefact;
    }

    public Artefact getArtefact(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Artefact artefact;
        String[] selectionArgs = { Integer.toString(id) };
        String selection = _ID+ " LIKE ? ";
        final Cursor cursor = db.query(TABLE_NAME, new String[] {_ID, COLUMN_ARTEFACT_NAME, COLUMN_ARTEFACT_BRAND, COLUMN_ARTEFACT_CATEGORIES, COLUMN_ARTEFACT_TECHNICAL_DETAILS, COLUMN_ARTEFACT_ID, COLUMN_ARTEFACT_WORKING, COLUMN_ARTEFACT_DESCRIPTION, COLUMN_ARTEFACT_YEAR, COLUMN_ARTEFACT_PICTURES, COLUMN_ARTEFACT_TIMEFRAME}, selection, selectionArgs, null, null, null);
        cursor.moveToFirst();
        artefact = cursorToArtefact(cursor);
        //System.out.println(artefact.toString());
        return artefact;
    }

    public List<Artefact> getArtefactForSearch(String query) {
        //System.out.println(query);
        SQLiteDatabase db = this.getWritableDatabase();
        List<Artefact> artefactList = new ArrayList<>();
        String[] selectionArgs = { query, query, query, query, query, query, query, query };
        String selection = COLUMN_ARTEFACT_NAME + " LIKE ? OR " + COLUMN_ARTEFACT_BRAND + " LIKE ? OR " + COLUMN_ARTEFACT_CATEGORIES + " LIKE ? OR " + COLUMN_ARTEFACT_TECHNICAL_DETAILS + " LIKE ? OR " + COLUMN_ARTEFACT_WORKING + " LIKE ? OR " + COLUMN_ARTEFACT_DESCRIPTION + " LIKE ? OR " + COLUMN_ARTEFACT_YEAR + " LIKE ? OR " + COLUMN_ARTEFACT_TIMEFRAME + " LIKE ? ";
        final Cursor cursor = db.query(TABLE_NAME, new String[] {_ID, COLUMN_ARTEFACT_NAME, COLUMN_ARTEFACT_BRAND, COLUMN_ARTEFACT_CATEGORIES, COLUMN_ARTEFACT_TECHNICAL_DETAILS, COLUMN_ARTEFACT_ID, COLUMN_ARTEFACT_WORKING, COLUMN_ARTEFACT_DESCRIPTION, COLUMN_ARTEFACT_YEAR, COLUMN_ARTEFACT_PICTURES, COLUMN_ARTEFACT_TIMEFRAME}, selection, selectionArgs, null, null, null);

        if(cursor != null) {
            //Si le cursor n'est pas null et peut movetofirst
            if(cursor.moveToFirst()) {
                cursor.moveToFirst();
                artefactList.add(cursorToArtefact(cursor));
                while(cursor.moveToNext()) {
                    artefactList.add(cursorToArtefact(cursor));
                }
            }
        }
        return artefactList;
    }

    /**
     * Fills ContentValues result from a Artefact object
     */
    private ContentValues fillSearch(Artefact artefact) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ARTEFACT_NAME, artefact.getName());
        values.put(COLUMN_ARTEFACT_BRAND,artefact.getBrand());
        values.put(COLUMN_ARTEFACT_CATEGORIES, artefact.getAllCategories());
        values.put(COLUMN_ARTEFACT_TECHNICAL_DETAILS, artefact.getAllTechnicalDetails());
        values.put(COLUMN_ARTEFACT_ID, artefact.getIdArtefact());
        values.put(COLUMN_ARTEFACT_WORKING, artefact.getWorking());
        values.put(COLUMN_ARTEFACT_DESCRIPTION, artefact.getDescription());
        values.put(COLUMN_ARTEFACT_YEAR, artefact.getYear());
        values.put(COLUMN_ARTEFACT_PICTURES, artefact.getAllPictures());
        values.put(COLUMN_ARTEFACT_TIMEFRAME, artefact.getAllTimeFrame());
        return values;
    }

    public boolean addArtefactToSearchTable(Artefact artefact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fillSearch(artefact);

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(SEARCH_TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Returns a cursor on all the artefacts of the search data base
     */
    public Cursor fetchAllArtefactsFromSearch() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SEARCH_TABLE_NAME, null,
                null, null, null, null, COLUMN_ARTEFACT_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllArtefactsFromSearch()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Fills ContentValues result from a Artefact object
     */
    private ContentValues fillCategories(Artefact artefact) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ARTEFACT_NAME, artefact.getName());
        values.put(COLUMN_ARTEFACT_BRAND,artefact.getBrand());
        values.put(COLUMN_ARTEFACT_CATEGORIES, artefact.getAllCategories());
        values.put(COLUMN_ARTEFACT_TECHNICAL_DETAILS, artefact.getAllTechnicalDetails());
        values.put(COLUMN_ARTEFACT_ID, artefact.getIdArtefact());
        values.put(COLUMN_ARTEFACT_WORKING, artefact.getWorking());
        values.put(COLUMN_ARTEFACT_DESCRIPTION, artefact.getDescription());
        values.put(COLUMN_ARTEFACT_YEAR, artefact.getYear());
        values.put(COLUMN_ARTEFACT_PICTURES, artefact.getAllPictures());
        values.put(COLUMN_ARTEFACT_TIMEFRAME, artefact.getAllTimeFrame());
        return values;
    }

    public boolean addArtefactToCategoriesTable(Artefact artefact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fillCategories(artefact);

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(CATEGORIES_TABLE_NAME, null, values, CONFLICT_IGNORE);

        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Returns a cursor on all the artefacts of the search data base
     */
    public Cursor fetchAllArtefactsCategories() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(CATEGORIES_TABLE_NAME, null,
                null, null, null, null, COLUMN_ARTEFACT_CATEGORIES +" ASC", null);

        Log.d(TAG, "call fetchAllArtefactsCategories()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }
}
