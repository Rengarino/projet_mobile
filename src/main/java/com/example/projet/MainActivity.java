package com.example.projet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projet.data.Artefact;
import com.example.projet.data.MuseumDbHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    public RecyclerView recyclerArtefact;
    public List<Artefact> listArtefact = new ArrayList<>();
    public MuseumDbHelper MDH;
    public CursorAdapter adapter = new CursorAdapter();
    public SearchView editSearch;
    public List<Artefact> listArtefactForSearch = new ArrayList<>();
    public boolean categoriesExists;
    public Cursor cursorPopulate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MDH = new MuseumDbHelper(this);
        MDH.dropTable(MDH.getReadableDatabase());
        new Worker().execute(listArtefact);

        recyclerArtefact = (RecyclerView) findViewById(R.id.recyclerArtefact);
        recyclerArtefact.setLayoutManager(new LinearLayoutManager(this));
        recyclerArtefact.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));


        final Button triAlpha = (Button) findViewById(R.id.buttonOrderAlpha);

        triAlpha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor cursor = MDH.fetchAllArtefacts();
                adapter.changeCursor(cursor);
                recyclerArtefact.setAdapter(adapter);
            }
        });

        final Button triChrono = (Button) findViewById(R.id.buttonOrderChrono);

        triChrono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor cursor = MDH.fetchAllArtefactsFromYear();
                adapter.changeCursor(cursor);
                recyclerArtefact.setAdapter(adapter);
            }
        });

        editSearch = (SearchView) findViewById(R.id.searchViewBar);
        editSearch.setOnQueryTextListener(this);

        final Button triCategorie = (Button) findViewById(R.id.buttonOrderCategorie);

        triCategorie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(categoriesExists) {
                    Cursor cursor = MDH.fetchAllArtefactsCategories();
                    adapter.changeCursor(cursor);
                    recyclerArtefact.setAdapter(adapter);
                }

                else {
                    orderByCategoriesProcess();
                    Cursor cursor = MDH.fetchAllArtefactsCategories();
                    adapter.changeCursor(cursor);
                    recyclerArtefact.setAdapter(adapter);
                }
            }
        });

    }

    public void orderByCategoriesProcess() {
        //Faire cette fonction seulement si la table n'existe pas
        MDH.dropCategoriesTable(MDH.getReadableDatabase());
        List<Artefact> artefactToChange = MDH.getAllArtefacts();
        List<Artefact> artefactChanged = new ArrayList<>();
        for(Artefact artefact : artefactToChange) {
            String categories = "";
            categories = artefact.getAllCategories();
            String[] eachCategorie = new String[categories.split(Pattern.quote("|")).length];

            //Pattern quote parce que le symbole est un caractère particulier
            eachCategorie = categories.split(Pattern.quote("|"));
            for(String categorie : eachCategorie) {
                //System.out.println(categorie);
                Artefact artefact2 = new Artefact(artefact.getId(), artefact.getName(), artefact.getBrand(), categorie, artefact.getTechnicalDetails(), artefact.getIdArtefact(), artefact.getWorking(), artefact.getDescription(), artefact.getYear(), artefact.getPictures(), artefact.getTimeFrame());
                artefactChanged.add(artefact2);
            }
        }

        for(Artefact artefact : artefactChanged) {
            MDH.addArtefactToCategoriesTable(artefact);
        }
        setCategoriesExists();
    }

    public void setCategoriesExists() {
        this.categoriesExists = true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data != null) {
            if(requestCode == 1) {
                return;
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adapter.filter(query);
        Cursor cursor = MDH.fetchAllArtefactsFromSearch();
        adapter.changeCursor(cursor);
        recyclerArtefact.setAdapter(adapter);
        Toast.makeText(this, "Recherche effectuée", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    class CursorAdapter extends RecyclerView.Adapter<RowHolder> {

       private Cursor adapterCursor;

       public Cursor changeCursor(Cursor cursor) {
           if(cursor == adapterCursor) {
               return null;
           }
           else {
               Cursor oldCursor = adapterCursor;
               if(oldCursor != null) oldCursor.close();
               adapterCursor = cursor;
               notifyDataSetChanged();
               return adapterCursor;
           }
       }

        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row_artefact, parent, false)));
        }

        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {
            if(!adapterCursor.moveToPosition(position))
            {
                throw new IllegalStateException("Couldn't move cursor to position " + position);
            }

            int ID = adapterCursor.getInt(adapterCursor.getColumnIndex(MDH._ID));
            holder.itemView.setTag(ID);
            holder.bindModel(adapterCursor);
        }

        @Override
        public int getItemCount() {
            return adapterCursor.getCount();
        }

        public void filter(String queryText) {
           if(queryText.isEmpty())
           {
               return;
           }
           else
           {
               String requete = "%"+queryText+"%";
               MDH.dropSearchTable(MDH.getReadableDatabase());
               listArtefactForSearch = MDH.getArtefactForSearch(requete);
               if(listArtefactForSearch != null) {
                   for (Artefact artefact : listArtefactForSearch) {
                       MDH.addArtefactToSearchTable(artefact);
                   }
               }
           }
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name = null;
        TextView categories = null;
        TextView marque = null;
        ImageView icon = null;
        int rowID = 0;

        RowHolder(View row) {
            super(row);
            name = (TextView) row.findViewById(R.id.textViewName);
            categories = (TextView) row.findViewById(R.id.textViewCategories);
            icon = (ImageView) row.findViewById(R.id.imageViewArtifact);
            marque = (TextView) row.findViewById(R.id.textViewMarque);
            row.setOnClickListener(this); //Waiting for a click
        }

        @Override
        public void onClick(View v) {
            Artefact artefact = MDH.getArtefact(rowID);
            //System.out.println(artefact.toString());
            Intent intent = new Intent(MainActivity.this, ArtefactActivity.class);
            intent.putExtra(Artefact.TAG, artefact);
            startActivityForResult(intent, 1);
        }

        void bindModel(Cursor cursor) {
            rowID = cursor.getInt(cursor.getColumnIndex(MDH._ID));
            name.setText(cursor.getString(cursor.getColumnIndex(MDH.COLUMN_ARTEFACT_NAME)));
            marque.setText(cursor.getString(cursor.getColumnIndex(MDH.COLUMN_ARTEFACT_BRAND)));
            String cat = "";
            if(cursor.getString(cursor.getColumnIndex(MDH.COLUMN_ARTEFACT_CATEGORIES)).substring(cursor.getString(cursor.getColumnIndex(MDH.COLUMN_ARTEFACT_CATEGORIES)).length()-1).equals("|"))
            {
                String categoriesToDisplay = cursor.getString(cursor.getColumnIndex(MDH.COLUMN_ARTEFACT_CATEGORIES)).substring(0, cursor.getString(cursor.getColumnIndex(MDH.COLUMN_ARTEFACT_CATEGORIES)).length() - 1);
                cat = categoriesToDisplay.replace("|", " & ");
            }
            else cat = cursor.getString(cursor.getColumnIndex(MDH.COLUMN_ARTEFACT_CATEGORIES));
            categories.setText(cat);
            File path = new File(MainActivity.this.getExternalFilesDir(null), cursor.getString(cursor.getColumnIndex(MDH.COLUMN_ARTEFACT_ID))+".png");
            if(path != null) {
                Bitmap myBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
                icon.setImageBitmap(myBitmap);
            }
            else icon = null;
        }
    }

    public class Worker extends AsyncTask<List<Artefact>, String, List<Artefact>> implements com.example.projet.Worker {

        Request request;
        AlertDialog.Builder cancelledAlert;

        @Override
        protected List<Artefact> doInBackground(List<Artefact>... lists) {
            request = new Request();
            request.getArtefactsInfo(listArtefact);
            for(Artefact artefact : listArtefact) {
                saveToInternalStorage(request.getThumbnailFromURL(artefact.getIdArtefact()), artefact.getIdArtefact());
            }
            return listArtefact;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cancelledAlert = new AlertDialog.Builder(MainActivity.this);
        }

        @Override
        protected void onPostExecute(List<Artefact> artefactList) {
            super.onPostExecute(artefactList);
            listArtefact = artefactList;
            for(Artefact artefact : listArtefact) {
                if(artefact.getYear() == 0) { //Si l'objet n'a pas de date d'apparition alors on prend sa première époque de parution
                    //System.out.println(artefact.toString());
                    String timeFrame = artefact.getTimeFrame();
                    String [] allTimeFrame = timeFrame.split("-");
                    artefact.setYear(Integer.valueOf(allTimeFrame[0]));
                    //System.out.println(artefact.toString());
                }
                MDH.addArtefact(artefact);
            }
            cursorPopulate = MDH.fetchAllArtefacts();
            adapter.changeCursor(cursorPopulate);
            recyclerArtefact.setAdapter(adapter);
        }
    }

    private void saveToInternalStorage(Bitmap icon, String artefactId)
    {
        File path = new File(this.getExternalFilesDir(null), artefactId+".png");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path);
            // Use the compress method on the BitMap object to write image to the OutputStream
            icon.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
