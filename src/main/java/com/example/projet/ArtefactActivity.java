package com.example.projet;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projet.data.Artefact;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ArtefactActivity extends AppCompatActivity {

    private TextView textArtefactName, textBrand, textYear, textDemos, textTimeFrame, textTechnical, textCategorie, textDescription, textWorking, textImageDesc;
    private ImageView imageArtefact;
    private Artefact artefact;
    List<String> allImages = new ArrayList<>();
    List<String> allImagesDescription = new ArrayList<>();
    private int imageNumber = 0;
    List<String> demoList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artefact);

        artefact = (Artefact) getIntent().getParcelableExtra(Artefact.TAG);
        //System.out.println(artefact.toString());

        textArtefactName = (TextView) findViewById(R.id.nameArtefact);
        textBrand = (TextView) findViewById(R.id.editBrand);
        textYear = (TextView) findViewById(R.id.editYear);
        textDemos = (TextView) findViewById(R.id.editDemos);
        textTimeFrame = (TextView) findViewById(R.id.editTimeFrame);
        textTechnical = (TextView) findViewById(R.id.editTechnical);
        textCategorie = (TextView) findViewById(R.id.editCategorie);
        textDescription = (TextView) findViewById(R.id.editDescription);
        textWorking = (TextView) findViewById(R.id.editEtat);
        textImageDesc = (TextView) findViewById(R.id.editImageDesc);

        imageArtefact = (ImageView) findViewById(R.id.thumbnailArtefact);

        new Worker().execute(artefact);

        updateView();

        final Button nextPicture = (Button) findViewById(R.id.buttonSwitchImage);
        nextPicture.setVisibility(View.GONE);
        nextPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNumber+=1;
                //Si le nombre ne dépasse pas le nombre d'images
                if(imageNumber < allImages.size())
                {
                    textImageDesc.setText(allImagesDescription.get(imageNumber-1));
                    File path = new File(ArtefactActivity.this.getExternalFilesDir(null), artefact.getName()+"_"+ imageNumber +".jpeg");
                    //System.out.println(path);
                    if(path != null) {
                        String pathput = path.getAbsolutePath();
                        Bitmap myBitmap = BitmapFactory.decodeFile(pathput);
                        imageArtefact.setImageBitmap(myBitmap);
                    }
                }
                //Sinon on revient à la première
                else
                {
                    imageNumber = 1;
                    textImageDesc.setText(allImagesDescription.get(imageNumber-1));
                    File path = new File(ArtefactActivity.this.getExternalFilesDir(null), artefact.getName()+"_"+ imageNumber +".jpeg");
                    if(path != null) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
                        imageArtefact.setImageBitmap(myBitmap);
                    }
                }
            }
        });


    }

    private void updateView() {
        textArtefactName.setText(artefact.getName());
        textBrand.setText(artefact.getBrand());
        textYear.setText(Integer.toString(artefact.getYear()));

        textTimeFrame.setText(artefact.getAllTimeFrame().substring(0, artefact.getAllTimeFrame().length()-1));

        String technicals = "";
        if (artefact.getAllTechnicalDetails() != null && artefact.getAllTechnicalDetails().length()>1) {
            technicals = artefact.getAllTechnicalDetails().substring(0, artefact.getAllTechnicalDetails().length()-1);
            String technicalsToDisplay = technicals.replace("|", " & ");
            technicals = technicalsToDisplay;
        }

        textTechnical.setText(technicals);

        String categoriesToDisplay = artefact.getAllCategories().substring(0, artefact.getAllCategories().length()-1);
        textCategorie.setText(categoriesToDisplay.replace("|", " & "));
        textDescription.setText(artefact.getDescription());
        String etat = artefact.getWorking();
        if(etat != null) {
            if (etat.equals("true")) etat = "Operationnel";
            else if (etat.equals("false")) etat = "Non operationnel";
        }
        else etat = "Inconnu";
        textWorking.setText(etat);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Toast.makeText(this,"Back key pressed", Toast.LENGTH_SHORT).show();
    }


    public class Worker extends AsyncTask<Artefact, String, Artefact> implements com.example.projet.Worker {

        final Button nextPicture = (Button) findViewById(R.id.buttonSwitchImage);
        Request request;
        AlertDialog.Builder cancelledAlert;
        String demo = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cancelledAlert = new AlertDialog.Builder(ArtefactActivity.this);
        }

        @Override
        protected void onPostExecute(Artefact artefact) {
            super.onPostExecute(artefact);
            //Si il y a des images
            if(allImages.size() > 0) {
                imageNumber = 1;
                File path = new File(ArtefactActivity.this.getExternalFilesDir(null), artefact.getName() + "_" + imageNumber + ".jpeg");
                if (path != null) {
                    //System.out.println(path);
                    String pathput = path.getAbsolutePath();
                    Bitmap myBitmap = BitmapFactory.decodeFile(pathput);
                    imageArtefact.setImageBitmap(myBitmap);
                }

                else imageArtefact = null;
                textImageDesc.setText(allImagesDescription.get(0));
                nextPicture.setVisibility(View.VISIBLE);
            }

            else if(allImages.size() == 1)
            {
                nextPicture.setVisibility(View.GONE);
            }

            //Sinon
            else {
                textImageDesc.setText("Pas d'image");
                imageArtefact = null;
            }

            if(demoList.get(0) != null) textDemos.setText(demoList.get(0));
        }

        @Override
        protected Artefact doInBackground(Artefact... artefacts) {
            request = new Request();


            String allPictures = artefact.getAllPictures();
            if(!allPictures.isEmpty())
            {
                //-1 sinon il créait une string et un bitmap vide avec le dernier séparateur
                allPictures = artefact.getAllPictures().substring(0, artefact.getAllPictures().length()-1);
                String imageID = "";
                String imageDescription = "";
                String[] eachPicture = new String[allPictures.split(Pattern.quote("|")).length];

                //Pattern quote parce que le symbole est un caractère particulier
                eachPicture = allPictures.split(Pattern.quote("|"));

                //Splitter en fonction du | pour avoir chaque image indépendamment puis avec -- pour séparé ID et description d'une image
                for(String picture : eachPicture) {
                    //System.out.println("AFFICHAGE PICTURE : " +picture);
                    String[] eachPictureInformation = new String[2];
                    eachPictureInformation = picture.split(Pattern.quote("--"));
                    if(eachPictureInformation.length>0) allImages.add(eachPictureInformation[0]);
                    if(eachPictureInformation.length>1) allImagesDescription.add(eachPictureInformation[1]);
                    else allImagesDescription.add(""); //Description vide
                }

                //Sauvegarder toutes les images de l'artefact
                if(allImages.size()>0) {
                    for (String id : allImages) {
                        //String value of sinon cela affichera 01 02 03 au lieu de 1 2 3
                        imageNumber += 1;
                        String imageName = artefact.getName() + "_" + String.valueOf(imageNumber);
                        saveToInternalStorage(request.getImageFromURL(artefact.getIdArtefact(), id), imageName);
                    }
                }
            }
            //On se remet sur la première image
            imageNumber = 1;

            //Get demo from artefact
            request.getDemos(artefact.getIdArtefact(), demoList);
            return artefact;
        }
    }

    private void saveToInternalStorage(Bitmap icon, String imageName)
    {
        File path = new File(this.getExternalFilesDir(null), imageName+".jpeg");
        //System.out.println(path);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path);
            // Use the compress method on the BitMap object to write image to the OutputStream
            if(icon != null) icon.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
