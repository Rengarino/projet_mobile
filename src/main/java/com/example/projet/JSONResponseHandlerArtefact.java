package com.example.projet;

import android.util.JsonReader;
import android.util.JsonToken;

import com.example.projet.data.Artefact;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSONResponseHandlerArtefact {
    private static final String TAG = JSONResponseHandlerArtefact.class.getSimpleName();

    private List<Artefact> artefactList;
    public boolean complete = false;
    private Artefact artefact;

    public JSONResponseHandlerArtefact(List<Artefact> artefactList) { this.artefactList = artefactList; }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readArtefacts(reader);
        } finally {
            reader.close();
        }
    }

    public void readArtefacts(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            artefact = new Artefact(name);
            readArrayArtefacts(reader);
            artefactList.add(artefact);
        }
        reader.endObject();

    }

    private void readArrayArtefacts(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("name")) {
                artefact.setName(reader.nextString());
            } else if (name.equals("brand")) {
                artefact.setBrand(reader.nextString());
            }
            else if(name.equals("technicalDetails")) {
                reader.beginArray();
                while(reader.hasNext()) {
                    artefact.setTechnicalDetails(reader.nextString());
                }
                reader.endArray();
            }
            else if(name.equals("categories")) {
                reader.beginArray();
                while(reader.hasNext()) {
                    artefact.setCategories(reader.nextString());
                }
                reader.endArray();
            }
            else if(name.equals("timeFrame")) {
                reader.beginArray();
                while(reader.hasNext()) {
                    artefact.setTimeFrame(String.valueOf(reader.nextInt()));
                }
                reader.endArray();
            }
            else if(name.equals("pictures")) {
                reader.beginObject();
                while(reader.hasNext()) {
                    String picture = reader.nextName() + "--" + reader.nextString();
                    artefact.setPictures(picture);
                }
                reader.endObject();
            }
            else if(name.equals("year")) {
                artefact.setYear(reader.nextInt());
            }
            else if(name.equals("working")) {
                artefact.setWorking(String.valueOf(reader.nextBoolean()));
            }
            else if(name.equals("description")) {
                artefact.setDescription(reader.nextString());
            }
            else {
                reader.skipValue();
            }
        }
        reader.endObject();
        complete = true; //If everything is done
    }
}
